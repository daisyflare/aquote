use std::{collections, io::{self, Write, BufWriter, BufReader}, fs::File, fmt, env, process};

use serde::{Deserialize, Serialize};

use rand::seq::SliceRandom;

use colored::Colorize;

type Record = collections::HashMap<String, Vec<Quote>>;

#[derive(Debug, Deserialize, Serialize)]
struct Quote {
    body: String,
    tag: String,
}

impl fmt::Display for Quote {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        const SEPARATOR: &str = "\n    ";
        let longest = self.body.lines().map(|x| x.trim().chars().count()).max().unwrap();
        let tag_len = self.tag.chars().count();
        let offset = longest - tag_len;
        write!(f, "{SEPARATOR}{}\n{SEPARATOR}{:>longest$}", self.body.replace('\r', "").replace('\n', SEPARATOR).green(), self.tag.yellow())
    }
}

impl Quote {
    fn new<B: Into<String>, T: Into<String>>(body: B, tag: T) -> Self {
        Self {
            body: body.into(),
            tag: tag.into()
        }
    }
}

fn main() {
    let cond = |x: String| x.as_str() == "--writer" || x.as_str() == "-w";
    if env::args().any(cond) {
        let mut t: Vec<_> = env::args().filter(|x| !cond(x.to_string())).collect();
        if t.len() - 1 != 4 {
            eprintln!("Must have four non-signal args—body, tag, language, and file! Got: {:#?}", t);
            process::exit(1);
        }
        let file = t.pop().unwrap();
        let lang = t.pop().unwrap();
        let tag = t.pop().unwrap();
        let body = t.pop().unwrap();
        writer(body, tag, lang, file);
    } else {
        let mut t: Vec<_> = env::args().collect();
        if t.len() - 1 != 1 {
            eprintln!("Must have path to quotes file!");
            process::exit(1);
        }
        reader(t.pop().unwrap());
    }
}

fn writer(body: String, tag: String, lang: String, file: String) {
    let mut r = read_record_from_file(&file).unwrap_or_default();

    let quote = Quote {
        tag,
        body
    };

    r.entry(lang).or_insert_with(Vec::default).push(quote);

    let mut writer = BufWriter::new(File::create(&file).unwrap());
    serde_json::to_writer(&mut writer, &r).unwrap();
    writer.flush().unwrap();

    println!("Wrote to {file}!")
}

fn read_record_from_file(path: &str) -> Option<Record> {
    let mut reader = BufReader::new(File::open(path).ok()?);
    serde_json::from_reader(&mut reader).ok()
}

fn reader(path: String) {
    let r = read_record_from_file(&path).unwrap();

    let mut rng = rand::thread_rng();
    let keys = r.keys().collect::<Vec<_>>();

    let lang = keys.choose(&mut rng).unwrap();

    let quote = r[*lang].choose(&mut rng).unwrap();

    println!("{}", quote);
}
