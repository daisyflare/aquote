#!/usr/bin/env python3

import argparse
import subprocess
import sys
import os

parser = argparse.ArgumentParser()
parser.add_argument("file")
parser.add_argument("--lang", default=None)
parser.add_argument("--outfile", default="./proof.json")
parser.add_argument("--replace", "-r", action='store_true')

args = parser.parse_args()

def handle_file(file: str) -> None:
    quotes = file.split("\n\n> ")
    current_lang = None
    postponed_lang = None
    for raw_quote in quotes:
        if raw_quote == "":
            continue
        lines = list(filter(lambda x: x.strip() != "", raw_quote.splitlines()))
        print(lines)
        tag = lines[-1]
        if len(lines) == 1:
            if lines[0].startswith("# "):
                current_lang = lines[0][1:].strip().lower()
                print("Lang is", current_lang)
                continue
            else:
                print("Items should have a tag!", file=sys.stderr)
                exit(1)
        if tag.startswith("# "):
            postponed_lang = lines[-1][1:].strip().lower()
            lines = lines[:-1]
            tag = lines[-1]
        body = ""
        for line in lines[:-1]:
            if line.startswith("# "):
                current_lang = line[1:].strip().lower()
                print("Lang is", current_lang)
                continue
            if line.startswith(">"):
                line = line[1:]
            body += ('\n' if body != "" else "") + line.strip()
        if args.lang is None and current_lang is None:
            print("No language provided in file or in arguments! Aborting", file=sys.stderr)
            exit(1)
        lang = args.lang if args.lang is not None else current_lang
        # type ignore
        assert not subprocess.call(["aquote", "--writer", body.strip(), tag, lang, args.outfile])
        if postponed_lang is not None:
            print("Lang is", postponed_lang)
            current_lang = postponed_lang
            postponed_lang = None

with open(args.file, "rt") as f:
    text = f.read()
    if args.replace:
        try:
            os.remove(args.outfile)
        except:
            pass
    handle_file(text)
