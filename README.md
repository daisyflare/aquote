# Aquote

Get quotes, right in your terminal!

## Install 

### Manual

``` sh
$ git clone https://www.gitlab.com/daisyflare/aquote
$ cd aquote
$ cargo build --release && cp target/release/aquote .
```

Requires [the Rust language](https://www.rustup.rs).

### AppImage

I have not set up the AppImage installation of this program yet, so please refer to the manual installation instructions.

## Using

Add a quote to a quotes file located at `file`, with body `body`, tag `tag`, under the language `language`: 

``` sh
$ aquote --writer <body> <tag> <language> <file>
```

Pick a random quote from file `file` and display it in terminal: 

``` sh
$ aquote <file>
```
